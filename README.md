# Javascript -> [Doxygen](http://www.stack.nl/~dimitri/doxygen)

* používat standardní způsob dokumentování kódu
* v __doxyfile__ nastavit __EXTENSION_MAPPING = js=C__

=========================================================

* use a standard method of documenting code
* in __doxyfile__ set value __EXTENSION_MAPPING = js=C__